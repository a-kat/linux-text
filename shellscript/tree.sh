#!/usr/bin/env bash

set -eu

list_recursive(){
	# localは、変数を関数外から使えなくできる
	# スクリプト内にもし同じ名前の変数があっても安全に使える
	local filepath=$1
	local indent=$2

	
	# ここでファイル名を出力している
	# ファイル名のみを出力する(/home/ubuntu/fileをfileとだけ出力したい)
	# そのため、##*/というパターンで、filepathのパス名部分を消している
	if [ -d "$filepath" ]; then
		# ファイルがディレクトリなら、ファイル名の最後に/を付ける
		echo "${indent}${filepath##*/}/"
	else
		echo "${indent}${filepath##*/}"
	fi

	# filepathがディレクトリなら、その中身も更にlist_recursiveの対象に(再帰)
	if [ -d "$filepath" ]; then
		# fnameはfilenameの略
		local fname
		# filepathディレクトリの中のファイルを全て対象にしてlist_recursive関数を使う
		for fname in $(ls "$filepath")
		do
			# indent変数に前回のインデントの大きさが保存されている
			list_recursive "${filepath}/${fname}" "    $indent"
		done
	fi
}

# list_recursiveを呼ぶ(1回目のみ、それ以降は再帰的に呼ばれる)
list_recursive "$1" ""
